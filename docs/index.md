# QuantumInformation.jl

Welcome to the documentation site of QuantumInformation.jl a quantum information processing toolkit written in julia.

## Installation

To install QuantumInformation.jl toolkit open a Julia REPL and type
```julia
julia> Pkg.clone("")
```

## Getting started

Loading QuantumInformation and creating a GHZ state for 4 particles is as easy as
```julia
julia> using QuantumInformation

julia> ghz(4)
```
